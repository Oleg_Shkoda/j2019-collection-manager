package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.impl.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.repository.impl.CollectionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ApplicationTests {

    private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
    private CollectionItemRepository itemRepository = new CollectionItemRepositoryImpl();


    @BeforeEach
    void setUp() {
        itemRepository.deleteAll();
        collectionRepository.deleteAll();
    }

    @Test
    void save_collectionWithoutItems() {

        //given

        Collection c = Collection.builder()
                .name("3")
                .title("4")
                .urlImage("6")
                .description("9")
                .build();

        Collection saved = collectionRepository.create(c);

        CollectionItem i1 = CollectionItem.builder().name("y1").collection(saved).build();
        CollectionItem i2 = CollectionItem.builder().name("y2").collection(saved).build();

        CollectionItem saved1 = itemRepository.create(i1);
        CollectionItem saved2 = itemRepository.create(i2);


        //when


        //then
        Assertions.assertNotNull(saved.getId());


    }

    @Test
    void findById_happyPth() {

        //given
        Collection c = Collection.builder()
                .name("cvb")
                .title("tyu")
                .urlImage("dfvc")
                .description("ijh")
                .build();

        Collection saved = collectionRepository.create(c);

        CollectionItem i1 = CollectionItem.builder().name("c1").collection(saved).build();
        CollectionItem i2 = CollectionItem.builder().name("c2").collection(saved).build();

        itemRepository.create(i1);
        itemRepository.create(i2);

        //when

        Collection found = collectionRepository.findById(saved.getId());

        //then

        Assertions.assertNotNull(found);
        Assertions.assertNotNull(found.getId());
        Assertions.assertNotNull(found.getItems());
        Assertions.assertEquals(2, found.getItems().size());

    }

    @Test
    void testCreate() {

        //given
        Collection collection1 = Collection.builder()
                .description("my-description5")
                .urlImage("my-image5")
                .title("my-title5")
                .name("my-name5")
                .build();

        Collection collection2 = Collection.builder()
                .description("my-description6")
                .urlImage("my-image6")
                .title("my-title6")
                .name("my-name6")
                .build();


        //when
        Collection saved1 = collectionRepository.create(collection1);
        Collection saved2 = collectionRepository.create(collection2);

        //then
        Assertions.assertNotNull(saved1.getId()); // проверка на ID не NULL
        Assertions.assertNotNull(saved2.getId()); // проверка на ID не NULL


    }

    @Test
    void testFindAll() {


        //given
        Collection collection1 = Collection.builder()
                .description("my-description3")
                .urlImage("my-image3")
                .title("my-title3")
                .name("my-name3")
                .build();

        Collection collection2 = Collection.builder()
                .description("my-description4")
                .urlImage("my-image4")
                .title("my-title4")
                .name("my-name4")
                .build();

        Collection saved1 = collectionRepository.create(collection1);
        Collection saved2 = collectionRepository.create(collection2);

        //when
        List<Collection> foundList = collectionRepository.findAll();

        //then
        Assertions.assertEquals(2, foundList.size()); //

    }

    @Test
    void test3() {

    }

    @Test
    void test4() {

    }

}

package by.itstep.collections.manager.dto.comment;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import lombok.*;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentFullDto {

    private Long id;
    private String message;
    private User user;
    private Collection collection;
    private Date createdAt;
}

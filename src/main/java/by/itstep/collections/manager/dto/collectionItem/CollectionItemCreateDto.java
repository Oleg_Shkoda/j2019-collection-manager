package by.itstep.collections.manager.dto.collectionItem;

import by.itstep.collections.manager.entity.Collection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectionItemCreateDto {

    private Long collectionId;
    private String name;
}

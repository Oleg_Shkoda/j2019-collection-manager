package by.itstep.collections.manager.dto.tag;

import by.itstep.collections.manager.entity.Collection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagFullDto {

    private Long id;
    private String name;
    private List<Collection> collections;
}

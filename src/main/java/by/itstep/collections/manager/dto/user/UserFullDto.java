package by.itstep.collections.manager.dto.user;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.enums.Role;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFullDto {

    private Long id;
    private String firstName;
    private String lastName;
    private Role role;
    private String email;
    private List<Collection> collections;
    private List<Comment> comments;
}

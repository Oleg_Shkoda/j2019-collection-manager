package by.itstep.collections.manager.dto.collection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectionUpdateDto {

    private Long id;
    private String name;
    private String title;
    private String description;

}

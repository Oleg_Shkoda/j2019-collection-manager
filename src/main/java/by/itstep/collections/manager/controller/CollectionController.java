package by.itstep.collections.manager.controller;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CollectionController {
/*
API:
1. Получить главную страницу
2. Получить страницу по ИД коллекции
3. Получить страницу с формой создания коллекции
4. Дать возможность создать коллекцию из форм
 */

    @Autowired // <-- Спринг, пожалуйста заполни это поле
    private CollectionService collectionService;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;


    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {
        List<CollectionPreviewDto> found = collectionService.findAll();
        model.addAttribute("value","Hello from thymeleaf");
        model.addAttribute("all_collections", found);
        model.addAttribute("logined", authService.getLoginedUser() != null);

        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/profile/{id}")
    public String openProfile(Model model, @PathVariable Long id) {
        if (!authService.isAuthenticated()){
            return "redirect:/registration";//если ктото не залогинился и ломится в профиль
        }
        if (!authService.getLoginedUser().getId().equals(id)){
            return "redirect:/index";//если ктото ломится в чужой профиль
        }

        UserFullDto found = userService.findById(id);
        model.addAttribute("user",found);
        model.addAttribute("collectionCreateDto",new CollectionCreateDto());

        return "profile";
    }


//    @RequestMapping(method = RequestMethod.GET,value = "/path/{var}/params")
//    public String getMainPage(@PathVariable Long var, @RequestParam String a,
//                              @RequestParam String b){
//
//        System.out.println("VAR: " + var);
//        System.out.println("a: " + a);
//        System.out.println("b: " + b);
//
//        return "index";
//    }

    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String openRegistrationPage(Model model){
        model.addAttribute("createDto",new UserCreateDto());
        model.addAttribute("loginDto",new UserLoginDto());
        return "sign-in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/create")
    public String createUser(UserCreateDto userCreateDto){
        UserFullDto saved = userService.create(userCreateDto);
        return "redirect:/registration";
    }



    @RequestMapping(method = RequestMethod.POST,value = "/users/login")
    public String login(UserLoginDto loginDto){
        authService.login(loginDto); // Если логин и пароль подошли, то он будет залогинен

        return "redirect:/profile/" + authService.getLoginedUser().getId(); //TODO
    }

    @RequestMapping(method = RequestMethod.POST,value = "/collections/create")
    public String saveCollection(CollectionCreateDto createDto){

        try {
            collectionService.create(createDto);
        } catch (InvalidDtoException e) {
            return "redirect:/oops";//TODO Cтраница с ошибкой
        }
        return "redirect:/profile/" + authService.getLoginedUser().getId();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/oops")
    public String showErrorPage(){
        return "funny-error";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/logout")
    public String logout(){
        authService.logout();
        return "redirect:/index";
    }

//    @RequestMapping(method = RequestMethod.POST, value = "/collections/{userId}/create")
//    public String createAppointment(@PathVariable Long userId, CollectionCreateDto createDto) {
//
//        createDto.setUserId(userId);
//        try {
//            collectionService.create(createDto);
//        } catch (InvalidDtoException e) {
//            e.printStackTrace();
//        }
//
//        return "redirect:/profile/" + authService.getLoginedUser().getId();
//    }
}
//reg login
//otobrazenie str
//sozd zapisei i td

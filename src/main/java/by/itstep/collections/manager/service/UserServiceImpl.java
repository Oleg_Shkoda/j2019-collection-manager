package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.entity.enums.Role;
import by.itstep.collections.manager.mapper.UserMapper;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.impl.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserPreviewDto> findAll() {
        List<User> found = userRepository.findAll();
        System.out.println("UserServiceImpl -> found " + found.size() + " users");
        List<UserPreviewDto> converted = userMapper.mapToDtoList(found);
        return converted;
    }

    @Override
    public UserFullDto findById(final Long id) {
        User entity = userRepository.findById(id);
        return userMapper.mapToDto(entity);
    }

    @Override
    public UserFullDto create(final UserCreateDto createDto) {
        User entity = userMapper.mapToEntity(createDto);
        entity.setRole(Role.USER);
        User saved = userRepository.create(entity);
        return userMapper.mapToDto(saved);
    }

    @Override
    public UserFullDto update(final UserUpdateDto updateDto) {
        User entity = userMapper.mapToEntity(updateDto);

        User existingEntity = userRepository.findById(updateDto.getId());
        entity.setRole(existingEntity.getRole());
        entity.setEmail(existingEntity.getEmail());
        entity.setCollections(existingEntity.getCollections());
        entity.setComments(existingEntity.getComments());

        User updated = userRepository.update(entity);
        return userMapper.mapToDto(updated);
    }

    @Override
    public void delete(final Long id) {
        userRepository.delete(id);
        System.out.println("UserServiceImpl -> user with id: " + id + " was deleted");
    }
}

package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.tag.TagCreateDto;
import by.itstep.collections.manager.dto.tag.TagFullDto;
import by.itstep.collections.manager.dto.tag.TagPreviewDto;
import by.itstep.collections.manager.dto.tag.TagUpdateDto;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.mapper.TagMapper;
import by.itstep.collections.manager.repository.TagRepository;
import by.itstep.collections.manager.repository.impl.TagRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TagMapper tagMapper;

    @Override
    public List<TagPreviewDto> findAll(List<Tag> entities) {
        List<Tag> found = tagRepository.findAll();
        System.out.println("TagServiceImpl -> found " + found.size() + " tags");

        List<TagPreviewDto> dtos = tagMapper.mapToDtoList(found);

        return dtos;
    }

    @Override
    public TagFullDto findById(final Long id) {
        Tag found = tagRepository.findById(id);
        System.out.println("TagServiceImpl -> found Tag " + found);

        TagFullDto dto = tagMapper.mapToDto(found);
        return dto;
    }

    @Override
    public TagFullDto create(final TagCreateDto createDto) {
        Tag toSave = tagMapper.mapToEntity(createDto);
        Tag saved = tagRepository.create(toSave);
        System.out.println("TagServiceImpl -> create collection " + saved);

        TagFullDto dto = tagMapper.mapToDto(saved);
        return dto;
    }

    @Override
    public TagFullDto update(final TagUpdateDto updateDto) {
        Tag toUpdate = tagMapper.mapToEntity(updateDto);
        Tag existingTag = tagRepository.findById(updateDto.getId());
        toUpdate.setCollections(existingTag.getCollections());

        Tag updated = tagRepository.update(toUpdate);
        System.out.println("TagServiceImpl -> update Tag " + updated);

        TagFullDto dto = tagMapper.mapToDto(updated);

        return dto;
    }

    @Override
    public void delete(final Long id) {

        tagRepository.delete(id);
        System.out.println("TagServiceImpl -> Tag with id: " + id + " was deleted");

    }
}

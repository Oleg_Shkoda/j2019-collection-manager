package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.tag.TagCreateDto;
import by.itstep.collections.manager.dto.tag.TagFullDto;
import by.itstep.collections.manager.dto.tag.TagPreviewDto;
import by.itstep.collections.manager.dto.tag.TagUpdateDto;
import by.itstep.collections.manager.entity.Tag;

import java.util.List;

public interface TagService {

    List<TagPreviewDto> findAll(List<Tag> entities);

    TagFullDto findById(Long id);

    TagFullDto create(TagCreateDto createDto);

    TagFullDto update(TagUpdateDto updateDto);

    void delete(Long id);
}
